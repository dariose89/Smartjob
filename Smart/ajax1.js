window.onload = function () {
    Cargar();    
}
function Registrar()
{
    var cost=$("#costo").val();
    var product=$("#producto").val();
    var preci = $("#precio").val();
    var stoc = $("#stock").val();
    //var tip = $("#tipo:checked").val();
    $.ajax({
        type: "POST",
        dataType: 'html',
        url: "registro1.php",
        data: "costo="+cost+"&producto="+product+"&precio="+preci+"&stock="+stoc,
        success: function(resp){
            $('#respuesta').html(resp);
            Limpiar();
            Cargar();
        }
    });
}   
function Eliminar(id)
{
     var id= $("#id").val()
        $.ajax({
			type:'POST',
			dataType: 'html',
			url:'eliminar1.php',
            data: "id="+id,
            success: function(resp){
				$('#respuesta').html(resp);
				Limpiar();
				Cargar();
		}
	});
}   
function Cargar()
{
    $('#datagrid1').load('consulta1.php');    
}
function Limpiar()
{
$("#costo").val("");
$("#producto").val("");
$("#precio").val("");
$("#stock").val("");
$("#id").val("");
$("#porcentaje").val("");
}
function Modificar()
{
    var id= $("#id").val();
    var cost = $("#costo").val();
    var product = $("#producto").val();
    var preci = $("#precio").val();
    var stoc = $("#stock").val();
    var stoc1= $("#stock1").val();
    //var tip = $("#tipod:checked").val();
    if ($("#tipod:checked").val()=="E")
    {stoc=stoc1-stoc}
    else {stoc= Number(stoc1) + Number(stoc);}

    $.ajax({
        type: "POST",
        dataType: 'html',
        url: "modificar1.php",
        data: "id="+id+"&costo="+cost+"&producto="+product+"&precio="+preci+"&stock="+stoc,
        success: function(resp){
            $('#respuesta').html(resp);
            Limpiar();
            Cargar();
        }
    });
}
function Buscar()
{
    var id= $("#id").val()
		$.ajax({
			url: 'buscar1.php',
			type: 'POST',
			dataType: 'json',
            data: "id="+id,
        }).done(function(respuesta){
            $('#costo').val(respuesta.costo);
            $('#producto').val(respuesta.producto);
            $('#precio').val(respuesta.precio);
            $('#stock').val(respuesta.stock);
            $('#stock1').val(respuesta.stock);
        });
        ;}
function Actualizar()
{
if ($("#tipode:checked").val()=="C")
{Actualizarcosto()}
else{Actualizarprecio()}
Cargar();
}
function Actualizarcosto()
{
    var porcentaje=$("#porcentaje").val();
    $.ajax({
        type: "POST",
        dataType: 'html',
        url: "actualizarcosto.php",
        data: "porcentaje="+porcentaje,
        success: function(resp){
            $('#respuesta').html(resp);
            Limpiar();
            Cargar();
        }
    });

}
function Actualizarprecio()
{
    var porcentaje=$("#porcentaje").val();
    $.ajax({
        type: "POST",
        dataType: 'html',
        url: "actualizarprecio.php",
        data: "porcentaje="+porcentaje,
        success: function(resp){
            $('#respuesta').html(resp);
            Limpiar();
            Cargar();
        }
    });

}
