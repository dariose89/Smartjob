window.onload = function () {
   fechahoy();
    Cargar();    
}
function fechahoy(){
    var fecha = new Date(); //Fecha actual
    var mes = fecha.getMonth()+1; //obteniendo mes
    var dia = fecha.getDate(); //obteniendo dia
    var ano = fecha.getFullYear(); //obteniendo año
    if(dia<10)
      dia='0'+dia; //agrega cero si el menor de 10
    if(mes<10)
      mes='0'+mes //agrega cero si el menor de 10
    document.getElementById('fecha').value=ano+"-"+mes+"-"+dia;
    document.getElementById('fecha1').value=ano+"-"+mes+"-"+dia;
    document.getElementById('fecha2').value=ano+"-"+mes+"-"+dia;

  
}
function Registrar()
{
    var id1=$("#id1").val();
    var fecha=$("#fecha").val();
    var concepto=$("#concepto").val();
    var tipo =$("#tipo:checked").val();
    var importe =$("#importe").val();
    var stoc1=$("#stock1").val();
    var cantidad=$("#cantidad").val();

    if ($("#tipo:checked").val()=="I")
    {cantidad=stoc1-cantidad}
    else {cantidad= Number(stoc1) + Number(cantidad);}

    $.ajax({
        type: "POST",
        dataType: 'html',
        url: "registro2.php",
        data: "fecha="+fecha+"&concepto="+concepto+"&tipo="+tipo+"&importe="+importe+"&cantidad="+cantidad+"&id1="+id1,
        success: function(resp){
            $('#respuesta').html(resp);
            Limpiar();
            Cargar();
        }
    });
}   
function Eliminar(id)
{
     var id= $("#id").val()
        $.ajax({
			type:'POST',
			dataType: 'html',
			url:'eliminar2.php',
            data: "id="+id,
            success: function(resp){
				$('#respuesta').html(resp);
				Limpiar();
				Cargar();
		}
	});
}   
function Cargar()
{
    $('#datagrid2').load('consulta2.php');    
}
function Filtrar() 
{
    var fecha1=$("#fecha1").val();
    var fecha2=$("#fecha2").val();
    $.ajax({
        type: "POST",
        dataType: 'html',
        url: "consulta5.php",
        data: "fecha1="+fecha1+"&fecha2="+fecha2+"&id="+id,
        success: function(resp){
            $('#datagrid2').html(resp);
        }
    });
}

function Calcular() 
{
var filas=document.querySelectorAll("#datagrid2 tbody tr");
var total=0;
var total1=0;
filas.forEach(function(e) {
var columnas=e.querySelectorAll("td");
var importe=parseFloat(columnas[4].textContent);
var tipo= (columnas[3].textContent);
    if (tipo== "I") 
    {total=total+importe}
    else{total1=total1+importe}
});
window.alert("Ingresos "+ total+  " Egresos " + total1);    
} 

function Volver(){
    Cargar();
    Limpiar();
}
function Limpiar()
{
fechahoy();
$("#concepto").val("");
$("#importe").val("");
$("#cantidad").val("");
$("#id").val("");
}
function Modificar()
{
    var id1=$("#id1").val();
    var id= $("#id").val();
    var concepto = $("#concepto").val();
    var tipo =$("#tipo:checked").val();
    var importe = $("#importe").val();
    var stoc1=$("#stock1").val();
    var cantidad=$("#cantidad").val();

    if ($("#tipo:checked").val()=="I")
    {cantidad=stoc1-cantidad}
    else {cantidad= Number(stoc1) + Number(cantidad);}

    $.ajax({
        type: "POST",
        dataType: 'html',
        url: "modificar2.php",
        data: "id="+id+"&concepto="+concepto+"&tipo="+tipo+"&importe="+importe+"&cantidad="+cantidad+"&id1="+id1,
        success: function(resp){
            $('#respuesta').html(resp);
            Limpiar();
            Cargar();
        }
    });
}

function Buscar()
{
    var id= $("#id").val()
		$.ajax({
			url: 'buscar2.php',
			type: 'POST',
			dataType: 'json',
            data: "id="+id,
        }).done(function(respuesta){
            $('#concepto').val(respuesta.concepto);
            $('#importe').val(respuesta.importe);
        });
        ;}
function mifuncion(id)
{
		$.ajax({
			url: 'buscarselect.php',
			type: 'POST',
			dataType: 'json',
            data: "id="+id,
        }).done(function(respuesta){
            $('#concepto').val(respuesta.concepto);
            $('#stock1').val(respuesta.cantidad);
            $('#id1').val(respuesta.id);
        });
        ;}
