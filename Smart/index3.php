<!DOCTYPE html>
<html lang="es">
<head>
    <title>Clientes</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style3.css" type="text/css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script src="ajax3.js"></script>
    <style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}
.open-button {
  background-color: #8db7fc;
  color: white;
  border: none;
  cursor: pointer;
  position:fixed;
  bottom: 520px;
  right: 50px;
  width: 170px;
  height:50px;
  font-size: 1.2em;
}
.form-popup {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 147px;
  border: 3px solid #f1f1f1;
  z-index: 9;
}
.form-container {
  width: 500px;
  padding: 10px;
  background-color: #3771c8;
}
.form-container input[type=text], .form-container input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}
.form-container input[type=text]:focus, .form-container input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}
.form-container .btn {
  background-color: #4CAF50;
  color: white;
  border: none;
  cursor: pointer;
  width: 100px;
  margin-bottom:10px;
  opacity: 0.8;
}
.form-container .cancel {
  background-color: red;
}
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
</style>
</head>
<body>
    <header>
            <h2>Clientes</h2>
            <a class="boton" href="index4.php" target="_blank">Historial de Cliente</a>
          <a class="boton" href="OrdenBlanco.php" target="_blank">Abrir Orden</a> 
<header>
<div class="form-popup" id="myForm">
  <form name="" onsubmit="return true" action="" class="form-container">
  <input type="text" id="id" name="id" placeholder="id" tabindex= "0" size="20" autocomplete= "off" onclick="Buscar()" onkeyup="Buscar()">
  <input type="text" id="dni" name="dni" placeholder="dni" tabindex= "0" size="20" autocomplete= "off" onclick="Buscar1()" onkeyup="Buscar1()">
  <input type="text" id="nombre" name="nombre" placeholder="apellido y nombre" tabindex= "0" size="20" autocomplete= "off">
  <input type="text" id="equipo" name="equipo" placeholder="equipo" tabindex= "0" size="20" autocomplete= "off">
  <input type="text" id="reparacion" name="reparacion" placeholder="reparacion" tabindex= "0" size="20" autocomplete= "off">
  <input type="text" id="presupuesto" name="presupuesto" placeholder="presupuesto" tabindex= "0" size="20" autocomplete= "off">
  <input type="text" id="telefono" name="telefono" placeholder="telefono" tabindex= "0" size="20" autocomplete= "off">
  <input type="text" id="imei" name="imei" placeholder="imei" tabindex= "0" size="20" autocomplete= "off">
  <label><input type="radio" id="arreglo" name="arreglo"value=S>SIN ARREGLO</label>                   
  <label><input type="radio" id="tipo" name="tipo"value=P checked >Pendiente</label> 
  <label><input type="radio" id="tipo" name="tipo"value=E >Entregado</label> 
  <label><input type="radio" id="tipo" name="tipo"value=H >Hecho</label>                   
  <br style="clear:both;">
  <button  onclick= "Registrar()"id="registrar"name= "registrar"tabindex="7"value="registrar">Guardar</button>
    <button  onclick="Modificar()"id="modificar"name="modificar"tabindex="8"value="modificar">Modificar</button>
    <button  onclick="Eliminar()"id="eliminar" name="eliminar" tabindex="9"value="eliminar">Eliminar</button>
    <button type="button" class="btn cancel" onclick="closeForm()">Cerrar</button>
  </form>
    <div id="respuesta"></div>
</div>
<script>
function openForm() {
  document.getElementById("myForm").style.display = "block";
}
function closeForm() {
  Limpiar();
  document.getElementById("myForm").style.display = "none";
}
</script>
    <div id="contenedor">
            </section>
                </div>   
        <div id="consultar">
            <section class="widget13">
                <h4 class="widgettitulo">Clientes</h4>
                <div class="datagrid3" id="datagrid3" ondblclick="openForm()"> 
                </div>  
            </section>
        </div>
    </div>     
</body>
</html> 