window.onload = function () {
    Cargar();    
}
function Registrar()
{
    var fech=$("#fecha").val()
    var agend = $("#agenda").val();
    var tip = $("#tipo:checked").val();
    $.ajax({
        type: "POST",
        dataType: 'html',
        url: "registro.php",
        data: "fecha="+fech+"&agenda="+agend+"&tipo="+tip,
        success: function(resp){
            $('#respuesta').html(resp);
            Limpiar();
            Cargar();
        }
    });
}   
function Eliminar(id)
{
     var id= $("#id").val()
        $.ajax({
			type:'POST',
			dataType: 'html',
			url:'eliminar.php',
            data: "id="+id,
            success: function(resp){
				$('#respuesta').html(resp);
				Limpiar();
				Cargar();
		}
	});
}   
function Cargar()
{
    $('#datagrid').load('consulta.php');    
}
function Limpiar()
{
$("#agenda").val("");
$("#tipo").val("N");
$("#id").val("");
}

function Modificar()
{
    var id= $("#id").val();
    var agend = $("#agenda").val();
    var tip = $("#tipo:checked").val();
    $.ajax({
        type: "POST",
        dataType: 'html',
        url: "modificar.php",
        data: "id="+id+"&agenda="+agend+"&tipo="+tip,
        success: function(resp){
            $('#respuesta').html(resp);
            Limpiar();
            Cargar();
        }
    });
}

function Buscar()
{
    var id= $("#id").val()
		$.ajax({
			url: 'buscar.php',
			type: 'POST',
			dataType: 'json',
            data: "id="+id,
        }).done(function(respuesta){
            $('#agenda').val(respuesta.agenda);
        });
        ;}
        